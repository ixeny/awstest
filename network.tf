# Create a VPC
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

variable "az" {
  default = ["eu-west-3a", "eu-west-3b"]
}


resource "aws_subnet" "subnet1a" {
  vpc_id            = aws_vpc.main.id
  availability_zone = "eu-west-3a"
  cidr_block        = "10.0.1.0/24"

  tags = {
    Name = "1a"
  }
}

resource "aws_subnet" "subnet2a" {
  vpc_id            = aws_vpc.main.id
  availability_zone = "eu-west-3a"
  cidr_block        = "10.0.2.0/24"

  tags = {
    Name = "2a"
  }
}

resource "aws_subnet" "subnet1b" {
  vpc_id            = aws_vpc.main.id
  availability_zone = "eu-west-3b"
  cidr_block        = "10.0.3.0/24"

  tags = {
    Name = "1b"
  }
}

resource "aws_subnet" "subnet2b" {
  vpc_id            = aws_vpc.main.id
  availability_zone = "eu-west-3b"
  cidr_block        = "10.0.4.0/24"

  tags = {
    Name = "2b"
  }
}


resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "igw"
  }
}

resource "aws_route" "r" {
  route_table_id         = aws_vpc.main.default_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw.id
}
