resource "aws_eip" "eip" {
  instance = aws_instance.web.id
  vpc      = true
}

resource "aws_security_group" "allow_all" {
  name        = "allow_all"
  description = "Allow All inbound traffic"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "All"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "All"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_all"
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = "grafana"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDTeZgIV4+Hq+jCuv5NPt8nfQ0VknmE/TbtBA5ce+36W0zkUhnLpIb14l1KxVlJMZrHH+rh07gUi/a3mAowVcwZw5/vet/DTow1BRRrHJtmE8uBevoL5rfdoKU4nZDjU/n3IbvpsbI3PdGoAiZatTVxq0G9YzhDJmtfim7SXej3MVV92XmdLtJdw4bdnv2Fkqyy2kEat2x/Vp+jpHnLNarPQP75l+y0BGDtCVdSr35uRZLu/8nZNWPh5HP6/uoJ8mrPFzs1l2YtzgvQYgK7SpTipDwTNaQCFgq2CPBVVFew5tw6Oa2NDUgQPK630K4d4las8nf1r7Tp9BHzUrfEx0qX sacinassim90@gmail.com"
}


resource "aws_instance" "web" {
  ami                    = "ami-0de12f76efe134f2f"
  instance_type          = "t3.micro"
  subnet_id              = aws_subnet.subnet1b.id
  vpc_security_group_ids = [aws_security_group.allow_all.id]
  key_name               = aws_key_pair.deployer.key_name
  #checkov:skip=CKV_AWS_8:ebs can be unencrypted 
  metadata_options {
    http_tokens                 = "required"
    http_put_response_hop_limit = "1"
  }

  tags = {
    Name = "server"
  }
}

